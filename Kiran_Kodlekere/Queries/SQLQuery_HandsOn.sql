-------------------------------------- DB used AdventureWorks2014	------------------------------------------------------------------

---------Below Query Assigns Rank, Dense-Rank, Row_number to the result set based on salling date --------------
SELECT [Name], SafetyStockLevel, DaysToManufacture, CONVERT(varchar, SellStartDate, 103) as [Selling Date],
RANK() OVER (ORDER BY SellStartDate DESC) as Stock_Rank,
DENSE_RANK() OVER (ORDER BY SellStartDate DESC) as Stock_Dense_Rank,
ROW_NUMBER() OVER (ORDER BY SellStartDate DESC) as [Row_Number]
FROM Production.Product





------------ Query partions result set into 20 groups based on price--------------------
/*
no of records per group = total no of records / Number of groups we want to form (20).
if there no complete divisibility then the remaining records will be put one in each group.
*/

SELECT  SalesOrderID, OrderQty, UnitPrice, ModifiedDate,
NTILE(20) OVER(ORDER BY UnitPrice) as Parting
FROM [Sales].[SalesOrderDetail]




----------------------------- LEAD AND LAG ------------------------------------------------------------------
/*
Lag access previous recod if there isn't one it puts NULL. Same goes for LEAD except LEAD access next record
Query below is partitioned by gender
*/
SELECT FirstName, LastName, Gender, HireDate, VacationHours,
LAG(VacationHours) OVER (PARTITION BY Gender ORDER BY HireDate) as LAG,
LEAD(VacationHours) OVER (PARTITION BY Gender ORDER BY HireDate) as LEAD
FROM [HumanResources].[Employee]
JOIN [Person].[Person]
ON [HumanResources].[Employee].BusinessEntityID=[Person].[Person].BusinessEntityID




------------------------------------- FIRST VALUE And LAST VALUE ---------------------------------------------------
/*
Below query fetches first and last value of HireDate based on the order it is sorted 
*/
SELECT FirstName, LastName,  JobTitle, Gender, HireDate, SickLeaveHours,
FIRST_VALUE(HireDate) OVER (PARTITION BY Gender ORDER BY JobTitle) as First_Value,
LAST_VALUE(HireDate) OVER (PARTITION BY Gender ORDER BY JobTitle) as Last_Value
FROM [HumanResources].[Employee]
JOIN [Person].[Person]
ON [HumanResources].[Employee].BusinessEntityID=[Person].[Person].BusinessEntityID




-------------------------------------------------- Aggregate Window Functions ----------------------------------------------------
/*
below query executes all the aggregate window functions with different possibilities.
For Example SUM() function is used to get the running Total
*/
SELECT FirstName, LastName, VacationHours,
SUM(VacationHours) OVER (PARTITION BY Gender ORDER BY Gender ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW ) as [Running Total],
AVG(VacationHours) OVER (PARTITION BY Gender ORDER BY Gender ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING ) as [Average],
COUNT(FirstName) OVER (PARTITION BY Gender ORDER BY Gender ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW ) as [Employee Count],
MIN(VacationHours) OVER (PARTITION BY Gender ORDER BY Gender ROWS BETWEEN 1 PRECEDING AND 1 FOLLOWING ) as [Minmum Value],
MAX(VacationHours) OVER (PARTITION BY Gender ORDER BY Gender ROWS BETWEEN 2 PRECEDING AND CURRENT ROW ) as [Maximum Value]
FROM [HumanResources].[Employee]
JOIN [Person].[Person]
ON [HumanResources].[Employee].BusinessEntityID=[Person].[Person].BusinessEntityID




---------------- Below query creates View, which contains product and its purchasing details -------------------------
ALTER VIEW vProduct_Purchasing_Details
AS
SELECT [Name], DueDate, OrderQty, ReceivedQty, UnitPrice 
FROM [Production].[Product]
JOIN [Purchasing].[PurchaseOrderDetail]
ON [Production].[Product].ProductID = [Purchasing].[PurchaseOrderDetail].ProductID

-----------To see what's in the created view execute below query -----------
SELECT * FROM vProduct_Purchasing_Details





---------------------- Stored Procedure & Case Statements---------------------------------
/* 
Below procedure accepts an integer value as input and returns the resultset based on the number we pass.
While calling the procedure we must pass a parameter Otherwise it throws an error
CASE Statements are used to determine that whether product is manufactured in time or not. And a message is displayed accordingly.
*/

ALTER PROC spProduct_Details_Qty_Wise @Qty int
AS
SELECT Name, DaysToManufacture, StartDate, EndDate, OrderQty,
CASE
	WHEN DATEDIFF(DAY, StartDate, EndDate) <= DaysToManufacture+10 THEN 'Production is done In Time' 
	ELSE 'TOO Late!!!!'
END as [Message]
FROM Production.Product 
JOIN [Production].[WorkOrder] 
ON Production.Product.ProductID = [Production].[WorkOrder].ProductID WHERE OrderQty = @Qty ORDER BY DaysToManufacture DESC

spProduct_Details_Qty_Wise 10


---------------------------------------------PIVOT & Derived Tables & Table Variable---------------------------------------------------
/*
Puts row level data to column level
In this Example, There were 4 types of Credit Card and for all it has exactly one credit card number  
Derived table is used to avoid duplication values in table, since original table has Card ID associated with it.
Table variable is used to store the Pivoted columns so that it can be used again for unpivoting as well
*/

CREATE table #T1 --TABLE
(
SuperiorCard Bigint, 
Vista BigInt, 
Distinguish BigInt, 
ColonialVoice BigInt, 
ExpMonth int, 
ExpYear int
)

INSERT INTO #T1
SELECT SuperiorCard, Vista, Distinguish, ColonialVoice, ExpMonth, ExpYear
FROM 
(
SELECT CONVERT(BigInt, CardNumber) as CardNumber, CardType, ExpMonth, ExpYear FROM [Sales].[CreditCard]
) as [Source Table]
PIVOT 
(
	SUM(CardNumber) FOR CardType in (SuperiorCard, Vista, Distinguish, ColonialVoice)
) as Pivot_Table
go
--For Pivot
SELECT  * 
FROM #t1


delete from #t1 where Id_new > 48

go
SELECT CardType, ExpMonth, ExpYear FROM @T1
UNPIVOT
(
CardNumber FOR CardType IN (SuperiorCard, Vista, Distinguish, ColonialVoice)
)as Table_2




--------------------------------- UDF and IF Statements ---------------------------------------------------------
/*
Below Function returns marital status based on the gender value supplied to the function
temporary table is created to store the result set
IF statements are used to determine the gender value and fetching resultset based on that
A parameter (m or f) must be specified while calling the function
*/
ALTER FUNCTION Employee_Personel_Details (@gender varchar(5))
RETURNS @Resultset TABLE(
	FirstName varchar(20) ,
	LastName varchar(20),
	Gender Varchar(6),
	MaritalStatus varchar(5)
)
AS
BEGIN
	if @gender = 'f' 
		BEGIN
			INSERT INTO @Resultset
			SELECT FirstName, LastName, Gender, MaritalStatus 
			FROM [HumanResources].[Employee]
			JOIN [Person].[Person]
			ON [HumanResources].[Employee].BusinessEntityID=[Person].[Person].BusinessEntityID WHERE Gender = @gender ORDER BY MaritalStatus
		END
	ELSE
		BEGIN
			INSERT INTO @Resultset
			SELECT FirstName, LastName, Gender, MaritalStatus 
			FROM [HumanResources].[Employee]
			JOIN [Person].[Person]
			ON [HumanResources].[Employee].BusinessEntityID=[Person].[Person].BusinessEntityID WHERE Gender = @gender ORDER BY MaritalStatus 
		END
	RETURN
END


SELECT * FROM Employee_Personel_Details('m')
















