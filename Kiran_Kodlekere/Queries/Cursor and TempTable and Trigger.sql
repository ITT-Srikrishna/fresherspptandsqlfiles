create table #temp1(ID int , Name Varchar(20)) 

insert into #temp1 values(3, 'ayush')
insert into #temp1 values(2, 'ajay')
insert into #temp1 values(8, 'sujan')
insert into #temp1 values(5, 'kiran')
insert into #temp1 values(7, 'akshay')

select * from #temp1

DECLARE @emp_id int ,@emp_name varchar(20)
DECLARE @cnt int = 0

DECLARE emp_cursor CURSOR FOR     
SELECT ID,Name    
FROM #temp1  

OPEN emp_cursor
WHILE (@cnt < 3)
BEGIN
SET @cnt = @cnt + 1;
	FETCH NEXT FROM emp_cursor     
INTO @emp_id, @emp_name

END

print convert(varchar(20), @emp_id) + ' ' + @emp_name

CLOSE emp_cursor
DEALLOCATE emp_cursor


--select %%physloc%% as Row_ID, ID, Name from #temp1





-----------------------------------------------------------------Trigger-------------------------------------------------------------------------------------------------

CREATE TABLE tbl_Employee(ID INT IDENTITY PRIMARY KEY, Name varchar(20), age int, salary float)
CREATE TABLE tbl_Employee_Audit(name varchar(20), salary float)

INSERT INTO tbl_Employee VALUES('Vijay', 20, 25000)
INSERT INTO tbl_Employee VALUES('kiran', 25, 35000)
INSERT INTO tbl_Employee VALUES('ajay', 30, 55000)
INSERT INTO tbl_Employee VALUES('sujan', 22, 28000)
INSERT INTO tbl_Employee VALUES('abhishek', 20, 30000)
INSERT INTO tbl_Employee VALUES('atrey', 20, 30000)
go
-- Trigger is created on Insert Event, On insert name and salary is inserted into audit table
ALTER TRIGGER tr_Employee_OnInsert
ON tbl_Employee
FOR INSERT  
AS
BEGIN

	DECLARE @name varchar(20), @sal float
	SELECT @name = Name, @sal = salary from inserted
	INSERT INTO tbl_Employee_Audit VALUES(@name, @sal)

END

INSERT INTO tbl_Employee VALUES('Gourish', 24, 45000)
INSERT INTO tbl_Employee VALUES('Girish', 23, 80000)
INSERT INTO tbl_Employee VALUES('Purvasha', 23, 50000)
INSERT INTO tbl_Employee VALUES('Reena', 23, 48000)
SELECT * FROM tbl_Employee_Audit

go


--- below query illustrates instead of trigger on Employee table -----------
CREATE TABLE tbl_Employee_Exit(name varchar(20), age int)


go
CREATE TRIGGER tr_Employee_OnDelete
ON tbl_Employee
FOR DELETE  
AS
BEGIN

	DECLARE @name varchar(20), @age int
	SELECT @name = Name, @age = age from deleted
	INSERT INTO tbl_Employee_Exit VALUES(@name, @age)
	
END

Delete tbl_Employee where ID = 2


select * from tbl_Employee
select * from tbl_Employee_Exit



