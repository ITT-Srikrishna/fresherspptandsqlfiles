use AdventureWorks2016;
select * from HumanResources.Department;
create table ##copy
(DepartmentID smallint primary key,
Name nvarchar(50) not null,
GroupName nvarchar(50) not null,
ModifiedDate datetime not null);

--Copying data from HumanResources.Department to temp table ##copy

insert into ##copy(DepartmentID,Name,GroupName,ModifiedDate)
select DepartmentID,Name,GroupName,ModifiedDate
from HumanResources.Department ;

--Displaying data

select * from ##copy;

--Table variable

declare @x table
(id int,
name varchar(100));
insert into @x values('Jyoti');
insert into @x values('Atul');
insert into @x values('Harish');
select * from @x;


--Performing operations on the temp table

select * from ##copy 
inner join HumanResources.Department 
on ##copy.DepartmentID=HumanResources.Department.DepartmentID;

select * from ##copy left outer join 
HumanResources.Department on 
##copy.DepartmentID=HumanResources.Department.DepartmentID;


