--DDL Commands

 --Create
 create table ##student(id int not null,Name varchar(10) not null);

 --alter

 alter table ##student add place varchar(20);
 select * from ##student;

 --truncate

 insert into ##student values(1,'Jyoti','Siliguri');
 truncate table ##student;

--DROP

drop table ##student;


--RENAME

 RENAME table ##student to ##student_info;  


 --DML Commands

 --Insert

 insert into ##student values(102,'Adam','Gangtok');
 insert into ##student values(103,'Eve','Delhi');
 insert into ##student values(104,'Sherlock','Jaipur');


 --Update

 update ##student set id=100 where Name='Jyoti';
 select * from student;

 update ##student set name='Avlanche',place='Gangtok' where id=102;

 --Delete

 --Deleting with a condition
 delete from ##student where id=103;

 --Deleting all records
 delete from ##student;


 --TCL Commands

 select * from ##student;

 --ROLLBACK

 begin transaction
 update ##student set Name='Mark' where id=103;
 rollback;
 select * from ##student;

 --SAVEPOINT

 begin transaction
 update ##student set name='James' where id=102;
 save transaction A;
 update ##student set Name='Jack' where id=103;
 save transaction B;
 rollback transaction A;
 select * from ##student;

 --COMMIT

 begin transaction
 update ##student set Name='Shark' where id=102;
 commit;
 select * from ##student;
