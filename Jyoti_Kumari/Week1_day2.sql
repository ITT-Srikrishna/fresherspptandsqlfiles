use AdventureWorks2016;

--DDL Commands

 --Create
 create table student(id int not null,Name varchar(10) not null);

 --alter

 alter table student add place varchar(20);
 select * from student;

 --truncate

 insert into student values(1,'Jyoti','Siliguri');
 truncate table student;
  select * from student;

--DROP

drop table student;


--RENAME

 RENAME table student to student_info;  


 --DML Commands

 --Insert

 insert into student values(102,'Adam','Gangtok');
 insert into student values(103,'Eve','Delhi');
 insert into student values(104,'Sherlock','Jaipur');
 select * from student;

 --Update

 update student set place='B.Street' where id=102;
 select * from student;

 update student set place='Bk.Street',Name='Sherlock.H' where id=102;

 --Delete

 --Deleting with a condition
 delete from student where id=104;

 --Deleting all records
 delete from student;


 --TCL Commands

 select * from HumanResources.Department;

 --ROLLBACK

 begin transaction
 update HumanResources.Department set GroupName='Sales and Marketing' where DepartmentID=2;
 rollback;
 select * from HumanResources.Department;

 --SAVEPOINT

 begin transaction
 update HumanResources.Department set GroupName='Sales and Marketing' where DepartmentID=2;
 save transaction A;
 update HumanResources.Department set GroupName='Research and Development' where DepartmentID=2;
 save transaction B;
 rollback transaction A;
 select * from HumanResources.Department;

 --COMMIT

 begin transaction
 update HumanResources.Department set GroupName='Research and Development' where DepartmentID=2;
 commit;
 select * from HumanResources.Department;


