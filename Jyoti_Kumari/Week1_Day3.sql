--SQL BUILT-IN FUNCTIONS

select CHARINDEX('cient','magnificient');
select CONCAT('jyoti','kumari','sah');
select ('jyoti'+'kumari'+'sah');
select DATALENGTH('A');
select DIFFERENCE('jyoti','kumari');
select FORMAT(1,'c','TH');
SELECT LEFT('jyoti',4);
SELECT LEN('jyoti');
select UPPER('JyOtI');
SELECT LTRIM('   jyoti');
select NCHAR(65);
select PATINDEX('_kumari_','jyotikumaris');
select REPLACE('jyotikumari','jyoti','pragya');
select REVERSE('jyoti');
SELECT RIGHT('JYOTIKUMARI',6);
SELECT RTRIM('   JYOTI    ');
SELECT SOUNDEX('KUMARI');
SELECT STR(500);
SELECT STUFF('JYOTI',3,8,'me0w');
select SUBSTRING('JYOTIKUMARI',6,3);

--NUMERIC FUNCTIONS

select abs(-10);
select floor(5.75);
select square(5);
select AVG(DepartmentID)  from HumanResources.Department;
select MIN(DepartmentID)  from HumanResources.Department;

--DATE FUNCTIONS

select getdate() as currentdatetime;
select DATEPART(NANOSECOND,getdate());
select DATEDIFF(month,1-01-2019,30-02-2019);
select CURRENT_TIMESTAMP;
select DAY('2019-03-07');
select MONTH('2019-03-07');
select year('2019-03-07');
SELECT CONVERT(VARCHAR(19),GETDATE()) ;
SELECT CONVERT(VARCHAR(10),GETDATE(),10) ;
SELECT CONVERT(VARCHAR(10),GETDATE(),110);

--ADVANCED FUNCTIONS

select CURRENT_USER;
select SYSTEM_USER;
select CAST('07-03-2019' as time);
select CONVERT(smalldatetime,'07-03-2019');
select FORMAT(getdate(),'dd-mm-yyyy');

