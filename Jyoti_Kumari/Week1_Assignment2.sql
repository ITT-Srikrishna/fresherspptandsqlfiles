--USING ISNULL(PERFORMING OPERARTIONS ON TEMP TABLE)

--TABLE FOR IMPLEMENTING
create table ##student
(ID INT,NAME VARCHAR(20),
HOUSE_NUMBER INT,
STREET_NAME VARCHAR(20),
CITY VARCHAR(20));

insert into ##student values(101,'Erica',204,'Chitrakoot','Jaipur');
insert into ##student values(102,'Adam',206,'Vaishali Nagar',NULL);
insert into ##student values(103,'Eve',208,'Bagdogra','Siliguri');

select * from ##student;

-- WITHOUT ISNULL

select *,NAME+' lives in'+CAST(HOUSE_NUMBER AS varchar)+','+STREET_NAME+','+CITY+' ' 
as FULL_ADDRESS_DETAILS
from ##student;

-- WITH ISNULL

select *,NAME+' lives in'+CAST(HOUSE_NUMBER AS varchar)+','+STREET_NAME+','+ISNULL(CITY,'Unknown City')+' ' 
as FULL_ADDRESS_DETAILS
from ##student;

--ANOTHER EXAMPLE

 SELECT ISNULL(NULL,'THE VALUE IS NULL');
 
 SELECT ISNULL('A NON-NULL VALUE','THE VALUE IS NULL');

--TURNING OFF CONCAT_NULL_YIELDS_NULL OFF

SET CONCAT_NULL_YIELDS_NULL OFF;


SET CONCAT_NULL_YIELDS_NULL ON;

--COALESCE
--FIRST NON NULL VALUE IN THE LIST WILL BE RETURNED

 SELECT  Coalesce(null,null,'W3SCHOOLS.COM',NULL); 
 SELECT  Coalesce(null,1,'W3SCHOOLS.COM',3);
 SELECT  Coalesce(null,null,NULL,50000);
 SELECT  Coalesce(null,null,NULL,GETDATE());




