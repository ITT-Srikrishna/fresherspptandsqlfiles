use AdventureWorks2016;

select * from HumanResources.Department;

--GROUP BY

select * from HumanResources.Department;
select GroupName,count(DepartmentID) 
as No_of_Departments from 
HumanResources.Department group by GroupName;


--ORDER BY

select * from HumanResources.Department order by Name;

select * from HumanResources.Department order by Name desc;

select DepartmentID,Name,GroupName from HumanResources.Department order by 3;


--AGGREGATE FUNCTIONS

--AVG

select AVG(DepartmentID) from HumanResources.Department;

--COUNT

select COUNT(Name) from HumanResources.Department;

--MIN

select MIN(DepartmentID) from HumanResources.Department;

--MAX

select AVG(DepartmentID) from HumanResources.Department;

--GROUPING

select GroupName,count(*) as grouping from HumanResources.Department group by GroupName;

--SUM

select SUM(DepartmentID) from HumanResources.Department;

--DEV

select STDEV(DepartmentID)  from HumanResources.Department;

--STDEVP

select STDEVP(DepartmentID)  from HumanResources.Department;

--VAR

select VAR(DepartmentID)  from HumanResources.Department;

--VARP

select VARP(DepartmentID)  from HumanResources.Department;


--GROUP BY

select * from HumanResources.Department;
select GroupName,count(DepartmentID) 
as No_of_Departments from 
HumanResources.Department group by GroupName;


--ORDER BY

select * from HumanResources.Department order by Name;

select * from HumanResources.Department order by Name desc;

select DepartmentID,Name,GroupName from HumanResources.Department order by 3;





