select CHARINDEX('cient','magnificient');
select CONCAT('jyoti','kumari','sah');
select ('jyoti'+'kumari'+'sah');
select DATALENGTH('A');
select DIFFERENCE('jyoti','kumari');
select FORMAT(1,'c','TH');
SELECT LEFT('jyoti',4);
SELECT LEN('jyoti');
select UPPER('JyOtI');
SELECT LTRIM('   jyoti');
select NCHAR(65);
select PATINDEX('_kumari_','jyotikumaris');
select REPLACE('jyotikumari','jyoti','pragya');
select REVERSE('jyoti');
SELECT RIGHT('JYOTIKUMARI',6);
SELECT RTRIM('   JYOTI    ');
SELECT SOUNDEX('KUMARI');
SELECT STR(500);
SELECT STUFF('JYOTI',3,8,'me0w');
select SUBSTRING('JYOTIKUMARI',6,3);
select abs(-10);
select floor(5.75);
select square(5);
select getdate() as currentdatetime;
select DATEPART(NANOSECOND,getdate());
select DATEDIFF(month,1-01-2019,30-02-2019);
select CURRENT_TIMESTAMP;
select DAY('2019-03-07');
select MONTH('2019-03-07');
select year('2019-03-07');
SELECT CONVERT(VARCHAR(19),GETDATE()) ;
SELECT CONVERT(VARCHAR(10),GETDATE(),10) ;
SELECT CONVERT(VARCHAR(10),GETDATE(),110);
select CURRENT_USER;
select SYSTEM_USER;
select CAST('07-03-2019' as time);
select CONVERT(smalldatetime,'07-03-2019');
select FORMAT(getdate(),'dd-mm-yyyy');



use AdventureWorks2016;
select * from HumanResources.Department;
select * from HumanResources.EmployeeDepartmentHistory;
select * from HumanResources.Shift;

select AVG(DepartmentID) from HumanResources.Department;
select count(DepartmentID) from HumanResources.Department;
select count(distinct DepartmentID) from HumanResources.Department;
select count_big(DepartmentID) from HumanResources.Department;

JOINS

select * from HumanResources.Department CROSS JOIN HumanResources.EmployeeDepartmentHistory;

select * from HumanResources.Department 
INNER JOIN HumanResources.EmployeeDepartmentHistory 
ON (HumanResources.Department.DepartmentID=HumanResources.EmployeeDepartmentHistory.DepartmentID);

select * from HumanResources.Department JOIN HumanResources.EmployeeDepartmentHistory;

select *,ISNULL(EmployeeDepartmentHistory.BusinessEntityID,0) from HumanResources.Department 
LEFT OUTER JOIN HumanResources.EmployeeDepartmentHistory
ON (HumanResources.Department.DepartmentID=HumanResources.EmployeeDepartmentHistory.ShiftID);


select (EmployeeDepartmentHistory.BusinessEntityID,0) from HumanResources.Department 
LEFT OUTER JOIN HumanResources.EmployeeDepartmentHistory
ON (HumanResources.Department.DepartmentID=HumanResources.EmployeeDepartmentHistory.ShiftID);


select * from HumanResources.Department 
RIGHT OUTER JOIN HumanResources.EmployeeDepartmentHistory
ON (HumanResources.Department.DepartmentID=HumanResources.EmployeeDepartmentHistory.BusinessEntityID);

select * from HumanResources.Department 
FULL OUTER JOIN HumanResources.EmployeeDepartmentHistory 
ON (HumanResources.Department.DepartmentID=HumanResources.EmployeeDepartmentHistory.ShiftID);

SUBQUERIES

select Name from HumanResources.Department 
where HumanResources.Department.DepartmentID 
NOT IN(select DepartmentID from HumanResources.EmployeeDepartmentHistory where 
ShiftID in(select ShiftID from HumanResources.Shift where Name='night'));



select * from HumanResources.Department;

--GROUP BY

select * from HumanResources.Department;
select GroupName,count(DepartmentID) 
as No_of_Departments from 
HumanResources.Department group by GroupName;


--ORDER BY

select * from HumanResources.Department order by Name;

select * from HumanResources.Department order by Name desc;

select DepartmentID,Name,GroupName from HumanResources.Department order by 3;




--AGGREGATE FUNCTIONS

--AVG

select AVG(DepartmentID) from HumanResources.Department;

--COUNT

select COUNT(Name) from HumanResources.Department;

--MIN

select MIN(DepartmentID) from HumanResources.Department;

--MAX

select AVG(DepartmentID) from HumanResources.Department;

--GROUPING

select GroupName,count(*) as grouping from HumanResources.Department group by GroupName;

--SUM

select SUM(DepartmentID) from HumanResources.Department;

--DEV

select STDEV(DepartmentID)  from HumanResources.Department;

--STDEVP

select STDEVP(DepartmentID)  from HumanResources.Department;

--VAR

select VAR(DepartmentID)  from HumanResources.Department;

--VARP

select VARP(DepartmentID)  from HumanResources.Department;


--GROUP BY

select * from HumanResources.Department;
select GroupName,count(DepartmentID) 
as No_of_Departments from 
HumanResources.Department group by GroupName;


--ORDER BY

select * from HumanResources.Department order by Name;

select * from HumanResources.Department order by Name desc;

select DepartmentID,Name,GroupName from HumanResources.Department order by 3;


















































































