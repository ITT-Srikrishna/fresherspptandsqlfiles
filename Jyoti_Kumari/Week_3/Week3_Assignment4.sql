--DEBUGGING SP WITH BREAKPOINTS AND PRINT STATEMENT

DECLARE @TargetNumber int
SET @TargetNumber=10
Execute spPrintEvenNumbers @TargetNumber
Print 'Done'


--sp

ALTER PROCEDURE spPrintEvenNumbers
@Target int
AS
BEGIN
    DECLARE @startnumber int
	SET @startnumber=0

	WHILE(@startnumber<=@Target)       --Breakpoint here
	BEGIN
	 IF(@startnumber%2=0)
	 Begin
	    Print @StartNumber
     End
	 Set @startnumber=@startnumber+1
	 END
	 PRINT 'FINISHED PRINTING'
END