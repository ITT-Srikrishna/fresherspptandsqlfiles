--CROSS APPLY AND OUTER APPLY

--TABLE VALUED FUNCTION 1

CREATE FUNCTION fn_getEmpByDept(@DeptId int)
RETURNS TABLE
AS
RETURN 
(
	SELECT * 
	FROM HumanResources.Employee AS E
	WHERE E.BusinessEntityID=@DeptId
)

--TABLE VALUED FUNCTION 2

CREATE FUNCTION fn_getEmpByDeptnew(@DeptId int)
RETURNS TABLE
AS
RETURN 
(
	SELECT * 
	FROM HumanResources.JobCandidate AS J
	WHERE J.JobCandidateID=@DeptId
)

--TABLES TO BE USED

select * from HumanResources.Department
select * from HumanResources.JobCandidate



--CROSS APPLY

SELECT D.DepartmentID,D.Name,E.NationalIDNumber
FROM HumanResources.Department D CROSS APPLY
fn_getEmpByDept(D.DepartmentID) E

--OUTER APPLY


SELECT D.DepartmentID,D.Name,J.JobCandidateID,J.Resume
FROM HumanResources.Department D OUTER APPLY
fn_getEmpByDeptnew(D.DepartmentID) J