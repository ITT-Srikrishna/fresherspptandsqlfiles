--RANKING AND ROW NUMBERING WITH PARTITION  BY

--CREATING A TABLE FOR TESTING

CREATE TABLE EMP
(EID INT,ENAME VARCHAR(30),GENDER VARCHAR(10),
SALARY MONEY,DEPTNO INT)

SELECT * FROM EMP

--ROW_NUMBER() WITH PARTITION BY

SELECT ENAME,SALARY,GENDER,ROW_NUMBER() 
OVER( PARTITION BY GENDER ORDER BY SALARY DESC) AS RANK
FROM EMP

--RANK()

SELECT ENAME,SALARY,RANK() 
OVER( PARTITION BY GENDER ORDER BY SALARY DESC) AS RANK
FROM EMP

--DENSE_RANK()

SELECT ENAME,SALARY,DENSE_RANK() 
OVER( PARTITION BY GENDER ORDER BY SALARY DESC) AS RANK
FROM EMP

--CLEANING THE DATABASE

DROP TABLE EMP