--USER DEFINED FUNCTIONS

--SCALAR FUNCTIONS

CREATE FUNCTION AddTwoNumbers
(
@a int,
@b int
)
RETURNS int
AS
BEGIN
RETURN @a + @b
END

PRINT dbo.AddTwoNumbers(10,30);

--INLINE TABLE-VALUED UDF

select * from HumanResources.Department;
CREATE FUNCTION GetName
( @Name nvarchar(20) )
RETURNS table
AS
RETURN (
SELECT DepartmentID,Name,GroupName
FROM HumanResources.Department
WHERE Name=@Name
)
GO


select * from GetName('Production');