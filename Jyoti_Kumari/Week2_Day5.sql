--TRY CATCH

BEGIN TRY
  SELECT 1/0;
END TRY
BEGIN CATCH
 SELECT 
     @@ERROR as Error,
	 ERROR_NUMBER() as ErrorNumber,
	 ERROR_SEVERITY()as ErrorSeverity,
	 ERROR_STATE() as ErrorState,
	 ERROR_LINE() as ErrorLine,
	 ERROR_PROCEDURE() as ErrorProcedure,
	 ERROR_MESSAGE() as ErrorMessage;
END CATCH;

BEGIN TRY
 declare @salary int;
 set @salary=50000;
 declare @name varchar(10);
 set @name='Jyoti';
 print @name+'earns'+@salary;  --TYPECASTING ERROR
END TRY
BEGIN CATCH
  SELECT 
     @@ERROR as Error,
	 ERROR_NUMBER() as ErrorNumber,
	 ERROR_SEVERITY()as ErrorSeverity,
	 ERROR_STATE() as ErrorState,
	 ERROR_LINE() as ErrorLine,
	 ERROR_PROCEDURE() as ErrorProcedure,
	 ERROR_MESSAGE() as ErrorMessage;
END CATCH
