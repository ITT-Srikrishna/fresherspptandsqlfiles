--IDENTITY SPECIFICATION

create table ##student
(ID INT IDENTITY(1,1),NAME VARCHAR(20),
HOUSE_NUMBER INT,
STREET_NAME VARCHAR(20),
CITY VARCHAR(20));


insert into ##student values('Erica',204,'Chitrakoot','Jaipur');
insert into ##student values('Adam',206,'Vaishali Nagar',NULL);
insert into ##student values('Eve',208,'Bagdogra','Siliguri');

select * from ##student;  

--SCOPE_IDENTITY

SELECT SCOPE_IDENTITY();         --LAST IDENTITY VALUE INSERTED INT THE SAME SCOPE WILL BE DISPLAYED

--IDENT_CURRENT

SELECT IDENT_CURRENT('##student'); --LAST IDENTITY VALUE GENERATED FOR A SPECIFIC TABLE WILL BE DISPLAYED