--USE OF CAST AND CONVERT FUNCTIONS

--CAST

SELECT CAST(25.5234 AS INT);
SELECT CAST('2019-03-15' AS datetime);
SELECT CAST('2019-03-15' AS time);

--CONVERT

SELECT CONVERT(VARCHAR(20),GETDATE(),6);      --Month Day YY
SELECT CONVERT(VARCHAR(20),GETDATE(),7);      --Month Day, YY
SELECT CONVERT(VARCHAR(20),GETDATE(),100);    --Month Day YYYY Time
SELECT CONVERT(VARCHAR(20),GETDATE(),106);    --Month Day YYYY
SELECT CONVERT(VARCHAR(20),GETDATE(),113);    --Month Day YYYY hh:mm:ss
SELECT CONVERT(VARCHAR(20),GETDATE());        --Month Day YYYY Time
SELECT CONVERT(VARCHAR(20),GETDATE(),108);    --hh:mm:ss