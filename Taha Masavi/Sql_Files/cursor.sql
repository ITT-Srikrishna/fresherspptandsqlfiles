use AdventureWorks2017
Go

declare @login nvarchar(256)
declare @id nvarchar(15)
Declare Cur CURSOR 
for
SELECT NationalIDNumber,LoginID from HumanResources.Employee

open cur

fetch next from cur into @id,@login 
print 'NationalIDNumber		LoginID'
while @@FETCH_STATUS = 0
begin
		print @id+'			'+@login+'			'
		fetch next from cur into @id,@login

end

close cur

deallocate cur