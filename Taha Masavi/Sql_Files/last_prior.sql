use Testing
go

declare @login varchar(65)
declare @id varchar(15)
Declare Cur CURSOR 
dynamic for
SELECT TestId,TestName from tbl_test

open cur

fetch last from cur into @id,@login 
print 'TESTID		TestName'
while @@FETCH_STATUS = 0
begin
		if @id='12'
			update tbl_test set TestName='test12' where current of cur
		else if @id='1'
			update tbl_test set TestName='test1' where current of cur 
			print @id + '			' +@login
		fetch prior from cur into @id,@login 
end

select * from tbl_test
close cur

deallocate cur
