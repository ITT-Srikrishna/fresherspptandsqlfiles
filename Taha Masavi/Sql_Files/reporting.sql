USE FreshersReporting
GO

CREATE SCHEMA Rpt
GO

CREATE TABLE Rpt.Tbl_Location
(
LocationID INT NOT NULL PRIMARY KEY IDENTITY(1,1),
LocationName NVARCHAR(25) NOT NULL)
GO

CREATE TABLE RPT.Tbl_FresherDetails(
FresherID INT NOT NULL PRIMARY KEY IDENTITY(1,1),
FresherName NVARCHAR(25) NOT NULL,
FresherAddress NVARCHAR(50) NOT NULL,
Technology NVARCHAR(25) NOT NULL,
StartDate DATE NOT NULL,
EndDate DATE,
Results NVARCHAR(25) NOT NULL DEFAULT 'NOT EVALUATED',
LocationID INT NOT NULL FOREIGN KEY REFERENCES Rpt.Tbl_Location(LocationID))
GO

CREATE TABLE Rpt.Tbl_Mentor(
MentorID INT NOT NULL PRIMARY KEY IDENTITY(1,1),
MentorName NVARCHAR(25) NOT NULL,
PhoneNumber nvarchar(15) NOT NULL)
GO

CREATE TABLE Rpt.Tbl_Manager(
ManagerID INT NOT NULL PRIMARY KEY IDENTITY(1,1),
ManagerName NVARCHAR(25) NOT NULL,
PhoneNumber nvarchar(15) NOT NULL)
GO

CREATE TABLE Rpt.Tbl_Ratings(
RatingID INT NOT NULL PRIMARY KEY IDENTITY(1,1),
FresherID INT NOT NULL REFERENCES Rpt.Tbl_FresherDetails(FresherID),
MentorID INT NOT NULL REFERENCES Rpt.Tbl_Mentor(MentorID),
ManagerID INT NOT NULL REFERENCES Rpt.Tbl_Manager(ManagerID),
Week INT NOT NULL,
CodingSkills INT NOT NULL,
InvestigationSkills int not null,
ProblemSolving INT NOT NULL,
IndependentThinking int not null,
PresentationSkills int not null,
Feedback nvarchar(250))
GO


