use TUTORIALS
Go
dec
/* updating the name as "jack" where the student id=9 in table student*/
Update students set Name='zack' where StudentId=9
GO

DELETE FROM class where ClassId=1
GO

SELECT * from students
GO

TRUNCATE table class
GO

Drop table class
GO

select * from students