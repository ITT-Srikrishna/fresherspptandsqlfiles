use AdventureWorks2017
go

select SalesOrderID,OrderQty,
SUM(OrderQty) over(partition by SalesOrderID) as Total,
AVG(OrderQty) over(partition by SalesOrderID) as Average,
MAX(OrderQty) over(partition by SalesOrderID) as Maximum,
MIN(OrderQty) over(partition by SalesOrderID) as Minimun,
COUNT(OrderQty) over(partition by SalesOrderID) as Count
FROM Sales.SalesOrderDetail

