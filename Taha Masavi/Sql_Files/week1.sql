USE [TUTORIALS]
GO
ALTER TABLE [dbo].[sub] DROP CONSTRAINT [FK__sub__StudentId__5CD6CB2B]
GO
/****** Object:  Table [dbo].[sub]    Script Date: 22-02-2019 17:10:49 ******/
DROP TABLE [dbo].[sub]
GO
/****** Object:  Table [dbo].[students]    Script Date: 22-02-2019 17:10:49 ******/
DROP TABLE [dbo].[students]
GO
/****** Object:  Table [dbo].[class]    Script Date: 22-02-2019 17:10:49 ******/
DROP TABLE [dbo].[class]
GO
/****** Object:  Table [dbo].[class]    Script Date: 22-02-2019 17:10:49 ******/
/** Create table statement for creating a table called class with elements ClassId as primary key,
ClassName and section **/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[class](
	[ClassId] [int] NOT NULL,
	[ClassName] [int] NOT NULL,
	[Section] [varchar](10) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ClassId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[students]    Script Date: 22-02-2019 17:10:49 ******/
/* Create table statement for creating a table students, having elements as StudentId as primary key,
Name and EmailId */
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[students](
	[StudentId] [int] NOT NULL,
	[Name] [varchar](25) NOT NULL,
	[Email] [varchar](30) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[StudentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sub]    Script Date: 22-02-2019 17:10:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* creating a table "sub" having elements SubjectId as primary key,
subjects and StudentId as foreign key referencing students(StudentId) */
CREATE TABLE [dbo].[sub](
	[SubjectId] [int] NOT NULL,
	[Subjects] [varchar](25) NULL,
	[StudentId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[SubjectId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/* inserting values in table "class" */
INSERT [dbo].[class] ([ClassId], [ClassName], [Section]) VALUES (1, 3, N'B')
INSERT [dbo].[class] ([ClassId], [ClassName], [Section]) VALUES (2, 8, N'A')
INSERT [dbo].[class] ([ClassId], [ClassName], [Section]) VALUES (3, 8, N'B')
INSERT [dbo].[students] ([StudentId], [Name], [Email]) VALUES (1, N'taha', N'B')
INSERT [dbo].[students] ([StudentId], [Name], [Email]) VALUES (2, N'sai', N'A')
INSERT [dbo].[students] ([StudentId], [Name], [Email]) VALUES (3, N'mark', N'B')
INSERT [dbo].[students] ([StudentId], [Name], [Email]) VALUES (4, N'john', N'B')
INSERT [dbo].[students] ([StudentId], [Name], [Email]) VALUES (6, N'taha', N'aa')
INSERT [dbo].[students] ([StudentId], [Name], [Email]) VALUES (9, N'taha', N'aa')
ALTER TABLE [dbo].[sub]  WITH CHECK ADD FOREIGN KEY([StudentId])
REFERENCES [dbo].[students] ([StudentId])
GO
