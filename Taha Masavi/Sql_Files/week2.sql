------------------------------Assignment 2-------------------------
create database Assignments
GO


Create table Students(
ID int Primary key Identity(1,1),
Name varchar(10),
Marks int)


Insert into Students(Name,Marks) values ('taha',80)
										('john',80),
										('sai',70),
										('bobby',76),
										('samantha',75),
										('ashley',84),
										('belvet',45)
										


select * from students where marks>60 order by right(Name,3)

--select * from students where marks > 75 order by REVERSE(substring(reverse(name),1,3))



--------------------------------HACKERRANK------------------------------
--Revising the Select Query I
select * from CITY where COUNTRYCODE='USA' and POPULATION>100000

--Revising the Select Query II
select NAME from CITY where COUNTRYCODE='USA' and POPULATION>120000;

--Select All
select * from CITY;

--Select By ID
SELECT * FROM CITY WHERE ID=1661

--Japanese Cities' Attributes
SELECT * FROM CITY WHERE COUNTRYCODE='JPN'

--Japanese Cities' Names
SELECT NAME FROM CITY WHERE COUNTRYCODE='JPN'

--Weather Observation Station 1
SELECT CITY,STATE FROM STATION

--Weather Observation Station 3
SELECT DISTINCT(CITY) FROM STATION WHERE ID%2=0

--Weather Observation Station 4
select (count(city)-count(distinct(city))) from station

--Weather Observation Station 6
select distinct(CITY) from STATION WHERE CITY LIKE "[aeiou]%"

--Weather Observation Station 7
select DISTINCT(CITY) from STATION WHERE CITY LIKE "%[aeiou]"

--Weather Observation Station 8
select DISTINCT(CITY) from STATION WHERE CITY LIKE "[aeiou]%[aeiou]"

--Weather Observation Station 9
select DISTINCT(CITY) from STATION WHERE CITY NOT LIKE "[aeiou]%"

--Weather Observation Station 10
select DISTINCT(CITY) from STATION WHERE CITY NOT LIKE "%[aeiou]"

--Weather Observation Station 12
select DISTINCT(CITY) from STATION WHERE CITY NOT LIKE "[aeiou]%" and CITY NOT LIKE "%[aeiou]"

--Higher Than 75 Marks
select name from STUDENTS where marks> 75 order by right(NAME,3),id

--Employee Names
select name from employee order by name

--Employee Salaries
select name from employee where months < 10 and salary >2000 order by employee_id

---------------------END OF HACKERRANK---------------------

--Printing NationalIDNumber and LoginID using cursor

use AdventureWorks2017
Go

declare @login nvarchar(256)
declare @id nvarchar(15)
Declare Cur CURSOR 
for
SELECT NationalIDNumber,LoginID from HumanResources.Employee

open cur

fetch next from cur into @id,@login 
print 'NationalIDNumber		LoginID'
while @@FETCH_STATUS = 0
begin
		print @id+'			'+@login+'			'
		fetch next from cur into @id,@login

end

close cur

deallocate cur

--------------------------------------------------

--Using Forward-Only Cursor to print TestId and TestName

use Testing
go

declare @login varchar(12)
declare @id varchar(15)
Declare Cur CURSOR 
FORWARD_ONLY for
SELECT TestId,TestName from tbl_test

open cur

fetch next from cur into @id,@login 
print 'TESTID		TestName'
while @@FETCH_STATUS = 0
begin
		print cast(@id as varchar(10)) + '			' + cast(@login as varchar(10))
		fetch next from cur into @id,@login
		
end

close cur

deallocate cur

---------------------------------------------------------

--use of Fetch Last in cursor to update the records. Updating records using if condition and iterating using while condition

use Testing
go

declare @login varchar(65)
declare @id varchar(15)
Declare Cur CURSOR 
dynamic for
SELECT TestId,TestName from tbl_test

open cur

fetch last from cur into @id,@login 
print 'TESTID		TestName'
while @@FETCH_STATUS = 0
begin
		if @id='12'
			update tbl_test set TestName='test12' where current of cur
		else if @id='1'
			update tbl_test set TestName='test1' where current of cur 
			print @id + '			' +@login
		fetch prior from cur into @id,@login 
end

select * from tbl_test
close cur

deallocate cur


-------------------------------------------

--Declaring Table Variable and printing the values

 DECLARE @tab table (ID int)
insert into @tab values (1),
						(2),
						(3)


select * from @tab  
insert into @tab values(6)
select * from @tab 

----------------------------------------
--DDL TRIGGER Restricting from Table drop or altering 

create trigger DDLTrigger
on database
for DROP_TABLE, ALTER_TABLE
as
begin
	PRINT 'DIABLE THE TRIGGER TO DO MODIFICATIONS'
	ROLLBACK
end

----------------System Functions---------
/* --NEWID()
-----NEWSEQUENTIALID()
-----HOST_ID()
-----HOST_NAME()
-----IDENTITY
*/
use AdventureWorks2017
go

create table products(
ProdID INT PRIMARY KEY IDENTITY(1,1),
ProdAltId UNIQUEIDENTIFIER NOT NULL DEFAULT NEWSEQUENTIALID(),
ProdName NVARCHAR(50),
AppId CHAR(10) NOT NULL DEFAULT HOST_ID(),
WorkStation NVARCHAR(128) NOT NULL DEFAULT HOST_NAME()
);



INSERT INTO products(ProdName) values (' ')

select * from products

alter table products
 add NEW UNIQUEIDENTIFIER DEFAULT NEWID()

 select NEWID()
  
  ---@@IDENTITY
  DELETE FROM Production.Location WHERE LocationID=@@IDENTITY 

-- @@ROWCOUNT
UPDATE HumanResources.Employee
SET JobTitle=N'executive'
WHERE NationalIDNumber=12343456789
IF @@ROWCOUNT = 0
PRINT 'NO ROWS WERE UPDATED'


--@@TRANCOUNT

BEGIN TRAN
	PRINT @@TRANCOUNT
	BEGIN TRAN
		PRINT @@TRANCOUNT
		COMMIT
		PRINT @@TRANCOUNT
COMMIT
PRINT @@TRANCOUNT

-----------------WINDOWS FUNCTION-------------------
-------AGGREGATE FUNCTIONS--------------

use AdventureWorks2017
go

select SalesOrderID,OrderQty,
SUM(OrderQty) over(partition by SalesOrderID) as Total,
AVG(OrderQty) over(partition by SalesOrderID) as Average,
MAX(OrderQty) over(partition by SalesOrderID) as Maximum,
MIN(OrderQty) over(partition by SalesOrderID) as Minimun,
COUNT(OrderQty) over(partition by SalesOrderID) as Count
FROM Sales.SalesOrderDetail

-----------------------------------------------
--------ROW_NUMBER----------
select *, ROW_NUMBER() over(partition by ModifiedDate order by SalesOrderID) as Row__Number
FROM Sales.SalesOrderDetail

--------------------------------------------

select SalesOrderID,OrderQty,RANK() over (partition by OrderQty order by SalesOrderId) as RANKINGS
from Sales.SalesOrderDetail

----------------------------------------

select SalesOrderID,OrderQty,DENSE_RANK() over (partition by OrderQty order by SalesOrderId) as RANKINGS
from Sales.SalesOrderDetail
----------------------------------------------------

select SalesOrderID,OrderQty,NTILE(4) over (partition by SalesOrderID order by SalesOrderId) as NTILE
from Sales.SalesOrderDetail
----------------------------------------------

---------------PIVOT---------------
SELECT 'AverageCost' AS Cost_Sorted_By_Production_Days,   
[0], [1], [2], [3], [4]  
FROM  
(SELECT DaysToManufacture, StandardCost   
    FROM Production.Product) AS SourceTable  
PIVOT  
(  
AVG(StandardCost)  
FOR DaysToManufacture IN ([0], [1], [2], [3], [4])  
) AS PivotTable;


---------------------------------

-- UNPIVOT-----------------  
CREATE TABLE pvt (VendorID int, Emp1 int, Emp2 int,  
    Emp3 int, Emp4 int, Emp5 int);  
GO  
INSERT INTO pvt VALUES (1,4,3,5,4,4);  
INSERT INTO pvt VALUES (2,4,1,5,5,5);  
INSERT INTO pvt VALUES (3,4,3,5,4,4);  
INSERT INTO pvt VALUES (4,4,2,5,5,4);  
INSERT INTO pvt VALUES (5,5,1,5,5,5);  
GO  
-- Unpivot the table.  
SELECT VendorID, Employee, Orders  
FROM   
   (SELECT VendorID, Emp1, Emp2, Emp3, Emp4, Emp5  
   FROM pvt) p  
UNPIVOT  
   (Orders FOR Employee IN   
      (Emp1, Emp2, Emp3, Emp4, Emp5)  
)AS unpvt;  
GO

------------LAG FUNCTION -----------------
USE AdventureWorks2017;  
GO  
SELECT BusinessEntityID, YEAR(QuotaDate) AS SalesYear, SalesQuota AS CurrentQuota,   
       LAG(SalesQuota) OVER (ORDER BY YEAR(QuotaDate)) AS PreviousQuota  
FROM Sales.SalesPersonQuotaHistory  
WHERE BusinessEntityID = 275;

----------------------------------------
-----------LEAD-------------------
SELECT BusinessEntityID, YEAR(QuotaDate) AS SalesYear, SalesQuota AS CurrentQuota,   
    LEAD(SalesQuota) OVER (ORDER BY YEAR(QuotaDate)) AS NextQuota  
FROM Sales.SalesPersonQuotaHistory  
WHERE BusinessEntityID = 275;
-------------------------------------------
----------------FIRST_VALUE------------

SELECT Name, ListPrice,   
       FIRST_VALUE(Name) OVER (ORDER BY ListPrice ASC) AS LeastExpensive   
FROM Production.Product  
WHERE ProductSubcategoryID = 37;

-----------------------




---------------------------------------
