use Testing
go

declare @login varchar(12)
declare @id varchar(15)
Declare Cur CURSOR 
FORWARD_ONLY for
SELECT TestId,TestName from tbl_test

open cur

fetch next from cur into @id,@login 
print 'TESTID		TestName'
while @@FETCH_STATUS = 0
begin
		print cast(@id as varchar(10)) + '			' + cast(@login as varchar(10))
		fetch next from cur into @id,@login
		
end

close cur

deallocate cur
