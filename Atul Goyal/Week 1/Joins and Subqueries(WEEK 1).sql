--JOINS

SELECT Count(P.ProductID) 
FROM SalesLT.Product as P 
CROSS JOIN SalesLT.ProductCategory as PC
WHERE P.ProductCategoryID=PC.ProductCategoryID

SELECT Count(P.ProductID) 
FROM SalesLT.Product as P 
INNER JOIN SalesLT.ProductCategory as PC
ON P.ProductCategoryID=PC.ProductCategoryID
ORDER BY PC.ProductCategoryID;


SELECT P.ProductID, PC.ProductCategoryID, PC.ParentProductCategoryID, PC.Name, PC.ModifiedDate 
FROM SalesLT.Product as P 
LEFT JOIN SalesLT.ProductCategory as PC
ON P.ProductCategoryID=PC.ProductCategoryID
ORDER BY PC.ProductCategoryID;

--ISNULL(iai.Quantity,0)

SELECT P.ProductID, PC.ProductCategoryID, PC.ParentProductCategoryID, PC.Name, PC.ModifiedDate 
FROM SalesLT.Product as P 
RIGHT JOIN SalesLT.ProductCategory as PC
ON P.ProductCategoryID=PC.ProductCategoryID
ORDER BY PC.ProductCategoryID;

SELECT ISNULL(P.ProductID,0), PC.ProductCategoryID, PC.ParentProductCategoryID, PC.Name, PC.ModifiedDate 
FROM SalesLT.Product as P 
RIGHT JOIN SalesLT.ProductCategory as PC
ON P.ProductCategoryID=PC.ProductCategoryID
ORDER BY PC.ProductCategoryID;

SELECT P.ProductID, PC.ProductCategoryID, PC.ParentProductCategoryID, PC.Name, PC.ModifiedDate 
FROM SalesLT.Product as P 
FULL JOIN SalesLT.ProductCategory as PC
ON P.ProductCategoryID=PC.ProductCategoryID
ORDER BY PC.ProductCategoryID;

--subqueries

SELECT ProductId, Name, Color
FROM SalesLT.Product
WHERE ProductId IN 
	(SELECT ProductId 
	FROM SalesLT.Product 
	WHERE Color='Red');

UPDATE SalesLT.Product
SET ListPrice=ListPrice * 1.5
WHERE ProductId IN
	(SELECT ProductId 
	FROM SalesLT.Product 
	WHERE Color='Red');

DELETE FROM SalesLT.Product
WHERE ProductId IN 
	(SELECT ProductId
	FROM SalesLT.Product 
	WHERE Name LIKE '%58');

SELECT P.ProductId, P.Name
FROM 
	(SELECT ProductID, Name
	FROM SalesLT.Product
	WHERE Color='Red') as P
WHERE P.ProductId > 750;

SELECT ProductId, Name
FROM SalesLT.Product
WHERE ProductCategoryID IN
	(SELECT ProductCategoryID 
	FROM SalesLT.ProductCategory
	WHERE Name IN 
		(SELECT Name 
		FROM SalesLT.ProductCategory
		WHERE Name LIKE '%a%')
	);

--Correlated subqueries

SELECT ProductCategoryID, Name, 
	(SELECT SUM(StandardCost) 
	FROM SalesLT.Product 
	WHERE ProductCategoryID=SalesLT.ProductCategory.ProductCategoryID) 
	AS StandardCost 
FROM SalesLT.ProductCategory;
