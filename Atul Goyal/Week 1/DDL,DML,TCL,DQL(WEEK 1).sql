USE AdventureWorksLT2014;
--DDL

--CREAT
CREATE TABLE Persons (
    ID int PRIMARY KEY IDENTITY(1, 1),
    LastName varchar(255) NOT NULL,
    FirstName varchar(255),
    Age int
);

--ALTER
ALTER TABLE Persons ADD Phone VARCHAR(10) NULL;

--TRUNCATE
TRUNCATE TABLE Persons;

--DROP
DROP TABLE Persons;

SELECT * FROM Persons;
--DML

--INSERT
INSERT INTO Persons (LastName, FirstName, Age)
VALUES ('A','B',4),
	('C','D',3),
	('E','F',2),
	('G','H',1);

--UPDATE
UPDATE Persons   
SET Phone='123456'
WHERE ID=1;

--DELETE
DELETE FROM Persons	
WHERE Age=1; 


--TCL

--COMMIT
BEGIN TRAN ;
DELETE FROM Persons
WHERE Age=2;
COMMIT TRAN ;

SELECT * FROM Persons;

--SAVE_POINT and ROLLBACK
BEGIN TRAN ;
SAVE TRAN ABC
DELETE FROM Persons
WHERE Age=2;
ROLLBACK TRAN ABC;

--DQL

SELECT * 
FROM Persons
WHERE Age BETWEEN 3 AND 4;