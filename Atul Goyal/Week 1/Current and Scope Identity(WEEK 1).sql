

CREATE TABLE tempT
(ID int IDENTITY(100,5) Primary key,
Name varchar(50) Not null);

INSERT tempT
VALUES ('A'),('B'),('C');

SELECT * FROM tempT;

--DROP TABLE tempT;

SELECT SCOPE_IDENTITY() AS SCOPE_IDENTITY;  --Return last Identity value in current session and scope

SELECT @@IDENTITY as IDENTITYY;				--Return last Identity value in current session and accross aal scope

SELECT IDENT_CURRENT ('tempT') AS Current_Identity;  --Return last Identity value for specific table in any session and scope



