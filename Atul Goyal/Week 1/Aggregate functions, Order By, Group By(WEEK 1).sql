--AGREEGATE FUNCTIONS

SELECT * FROM SalesLT.Product;

SELECT COUNT(Color)
FROM SalesLT.Product
WHERE Color='NULL';

SELECT DISTINCT Size
FROM SalesLT.Product;

SELECT DISTINCT Color
FROM SalesLT.Product;

--SELECT APPROX_COUNT_DISTINCT(Color)
--FROM SalesLT.Product;

--SELECT CHECKSUM_AGG(CAST(StandardCost AS int))
--FROM SalesLT.Product;

SELECT AVG(StandardCost)
FROM SalesLT.Product;

SELECT SUM(StandardCost)
FROM SalesLT.Product;

SELECT MAX(StandardCost)
FROM SalesLT.Product;

SELECT MIN(StandardCost)
FROM SalesLT.Product;

SELECT STDEV(StandardCost)
FROM SalesLT.Product;

SELECT VAR(StandardCost)
FROM SalesLT.Product;

--ORDER BY and GROUP BY

--GROUNPING BY 2 COLUMN
SELECT COLOR,Size, Count(Size)
FROM SalesLT.Product
Group By Color, Size
Order by Color;

--USE of HAVING CLAUSE
SELECT COUNT(P.ProductID), P.Color, P.Size
From SalesLt.Product AS P
GROUP BY P.Size, P.Color
HAVING P.Size='M';



