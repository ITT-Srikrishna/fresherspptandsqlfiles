--CAST

-- Type casting of FLOAT to int and DECIMAL Using CAST Function
SELECT 90.000005 AS Original, CAST(90.000005 AS int) AS INTEGER,
CAST(90.000005 AS decimal(6,4)) AS decimal;

--Retriveing Substing of Name From Product Table and 
--Casting ListPrice in int which has 3 in the beigining
SELECT SUBSTRING(Name, 1, 10) AS NAME, ListPrice, CAST(ListPrice AS int)
FROM SalesLT.Product
WHERE ListPrice LIKE '3%';


-- Type casting of FLOAT to int and DECIMAL Using Convert Function
SELECT 90.000005 AS Original, CONVERT(int, 90.000005) AS INTEGER,
CONVERT(decimal(6,4), 90.000005) AS decimal;

--Retriveing Substing of Name From Product Table and 
--Converting ListPrice in int which has 3 in the beigining
SELECT SUBSTRING(Name, 1, 10) AS NAME, ListPrice, CONVERT(int, ListPrice)
FROM SalesLT.Product
WHERE ListPrice LIKE '3%';

DECLARE @date DATE = '2019-03-15';
SELECT CONVERT(varchar, @date , 101);  --Changing date format in style 101

DECLARE @date1 DATE = '2006-12-29';
SELECT FORMAT (@date1,'ddd,MMMM,yyyy'); --CASTING FROM '2006-12-29' to Fri,Dec,2006




