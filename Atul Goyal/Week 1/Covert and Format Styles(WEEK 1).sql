--Convert Function 

SELECT GETDATE();

DECLARE @date DATE = '2019-03-15';		--Declaring date variable
DECLARE @time TIME = '20:28:22.140';	--Declaring time variable

SELECT CONVERT(varchar, @date , 100);	--Default date format

SELECT CONVERT(varchar, @date , 101);	--US date format

SELECT CONVERT(varchar, @date , 102);	--ANSI date format

SELECT CONVERT(varchar, @date , 103);	--British date format

SELECT CONVERT(varchar, @date , 104);	--German date format

SELECT CONVERT(varchar, @date , 105);	--Italian date format

SELECT CONVERT(varchar, @date , 106);	--dd mon yyyy

SELECT CONVERT(varchar, @date , 107);	--Mon dd, yyyy

SELECT CONVERT(varchar, @time , 108);	--hh:mm:ss

SELECT CONVERT(varchar, @date , 109);	--Default + millisec

SELECT CONVERT(varchar, @date , 110);	--USA date format

SELECT CONVERT(varchar, @date , 111);	--Japan date format

SELECT CONVERT(varchar, @date , 112);	--ISO date format

SELECT CONVERT(varchar, @date , 113);	--Europe (24 hour clock)

SELECT CONVERT(varchar, @time , 114);	--24 hour clock

SELECT CONVERT(varchar, @date , 120);	--Date and 24 hour clock


--FORMAT Function

DECLARE @date DATE = '2019-03-15';	

SELECT FORMAT(@date, 'd', 'en-US');	--US English Result
SELECT FORMAT(@date, 'd', 'en-gb');	--Great Britain English Result
SELECT FORMAT(@date, 'd', 'de-dn');	--German Result

DECLARE @date DATE = '2019-03-15';	

SELECT FORMAT (@date, 'D', 'en-US');	--US English Result
SELECT FORMAT (@date, 'D', 'en-gb');	--Great Britain English Result
SELECT FORMAT (@date, 'D', 'de-dn');	--German Result

SELECT FORMAT(123456789,'###-##-####');	--Custom Number Result

SELECT FORMAT(cast('07:35' as time), N'hh\.mm');  --> returns 07.35  
SELECT FORMAT(cast('07:35' as time), N'hh\:mm');  --> returns 07:35  