--Window Functions

--Creating window over the column Color and applying window function AVERAGE on StandardCost
SELECT ProductID, Name, StandardCost, Color,
	AVG(StandardCost) OVER(PARTITION BY Color ORDER BY Color ROWS 
	BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS AVERAGE
FROM Production.Product
WHERE Color IS NOT NULL;



--Creating window over the column Color on 1 PRECEDING AND 1 FOLLOWING 
--and applying window function SUM and COUNT on StandardCost
SELECT ProductID, Name, StandardCost, Color,
	Sum(StandardCost) OVER(PARTITION BY Color ORDER BY Color ROWS 
	BETWEEN 1 PRECEDING AND 1 FOLLOWING) AS SUM,
	Count(StandardCost) OVER(PARTITION BY Color ORDER BY Color ROWS 
	BETWEEN 1 PRECEDING AND 1 FOLLOWING) AS Count
FROM Production.Product
WHERE Color IS NOT NULL;


--Creating window over the column Color and applying window function MAXIMUM on StandardCost
SELECT DISTINCT(Color),
	MAX(StandardCost) OVER(PARTITION BY Color ORDER BY Color) AS MAXIMUM
FROM Production.Product
WHERE Color IS NOT NULL;


--Example for Running row count
SELECT ProductID, Name, StandardCost, Color,
	Count(StandardCost) OVER(PARTITION BY Color ORDER BY ProductID) AS Count
FROM Production.Product
WHERE Color IS NOT NULL;

