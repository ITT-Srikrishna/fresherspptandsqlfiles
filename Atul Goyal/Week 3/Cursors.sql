--CURSORS

SELECT * FROM HumanResources.Employee;


DECLARE @ID int, @Name nvarchar(50);

DECLARE DeptCursor CURSOR FOR									--CURSOR Declaration
SELECT DepartmentID, Name FROM HumanResources.Department;
Open DeptCursor													--Open CURSOR

FETCH NEXT FROM DeptCursor INTO @ID, @Name						--FETCH DepartmentID and Name in @ID and @Name

WHILE (@@FETCH_STATUS = 0)
BEGIN
	
	PRINT 'ID = ' + CONVERT(nvarchar(10),@ID) + 'Name = ' + @Name;
	FETCH NEXT FROM DeptCursor INTO @ID, @Name;
END

CLOSE DeptCursor;												--CLOSE Cursor
DEALLOCATE DeptCursor;											--DEALLOCATE Cursor
	

SELECT * FROM Production.Product;
ALTER Table Production.Product
ADD Rates int;

--NEW Cursor

DECLARE @Days int; 
DECLARE ProductCursor CURSOR FOR								--CURSOR Declaration
SELECT DaysToManufacture FROM Production.Product;
Open ProductCursor												--Open CURSOR

FETCH NEXT FROM ProductCursor INTO @Days						--FETCH DaysToManufacture and Name in @Days

WHILE (@@FETCH_STATUS = 0)
BEGIN
	--SELECT @Days = DaysToManufacture FROM Production.Product WHERE ;
	
	if(@days = 1)
	BEGIN
		UPDATE Production.Product SET Rates =100 WHERE DaysToManufacture=@Days;
	END
	else if(@days = 2)
	BEGIN
		UPDATE Production.Product SET Rates =200 WHERE DaysToManufacture=@Days;
	END
	else if(@days = 4)
	BEGIN
		UPDATE Production.Product SET Rates =400 WHERE DaysToManufacture=@Days;
	END

	FETCH NEXT FROM ProductCursor INTO @Days;

END

CLOSE ProductCursor;											--CLOSE Cursor
DEALLOCATE ProductCursor;										--DEALLOCATE Cursor


SELECT ProductID, Name, DaysToManufacture, Rates FROM Production.Product;


