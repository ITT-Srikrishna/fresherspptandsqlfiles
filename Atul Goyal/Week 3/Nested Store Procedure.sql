--Nested Store Procedure

CREATE PROC spProduct_1
@Color varchar(20),
@Size varchar(10)
AS
BEGIN
	SELECT ProductID,Name,Color,Size
	FROM Production.Product
	WHERE Color=@Color AND Size=@Size;
END

CREATE PROC spProduct_2
AS
BEGIN
	EXEC spProduct_1 'Black','L';
END

EXEC spProduct_2;



CREATE PROC spProduct_3
@Color nvarchar(20)
AS
BEGIN
	RETURN(SELECT MAX(StandardCost)
	FROM Production.Product
	WHERE Color=@Color);
END

ALTER PROC spProduct_4
AS 
BEGIN
	DECLARE @MaxCost int;
	EXEC @MaxCost=spProduct_3 'Black';
	RETURN (@MaxCOST);
END

DECLARE @MAX int
EXEC @MAX= spProduct_4;
Print @MAX