--UDF

CREATE FUNCTION CalcAge (@date date)
Returns int
AS
BEGIN
	DECLARE @Age int;
	SET @Age=DATEDIFF(year,@date,GETDATE()) -
		CASE	
			WHEN (MONTH(@date)) > (MONTH(GETDATE())) OR
				 (MONTH(@date)) = (MONTH(GETDATE())) AND (DAY(@date)) > (DAY(GETDATE()))
			THEN 1
			ELSE 0
		END
	RETURN @Age;
END

SELECT dbo.CalcAge('1996-10-24') AS Age;

SELECT * FROM HumanResources.Department

CREATE FUNCTION CalcAge (@Color nvarchar(15))
Returns Table
AS
	RETURN(SELECT ProductId, Name, Color
			FROM Production.Product
			WHERE Color=@Color);

CREATE FUNCTION GetName(@Name nvarchar(20))				--Function returns a table
RETURNS table
AS
	RETURN (SELECT DepartmentID,Name,GroupName
	FROM HumanResources.Department
	WHERE Name=@Name);

SELECT * FROM dbo.GetName('Sales');