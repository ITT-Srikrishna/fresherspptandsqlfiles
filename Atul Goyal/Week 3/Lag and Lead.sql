SELECT * FROM CountrySale


SELECT *,
	LEAD(SaleTotal) OVER(ORDER BY SaleTotal)  AS Lead
FROM CountrySale;

SELECT *,
	Lag(SaleTotal) OVER(ORDER BY SaleTotal)  AS Lag
FROM CountrySale;



SELECT *,
	Lead(SaleTotal,1,-1) OVER(PARTITION BY SalesAgent ORDER BY SaleTotal)  AS Lead
FROM CountrySale;

SELECT *,
	Lag(SaleTotal,1,-1) OVER(PARTITION BY SalesAgent ORDER BY SaleTotal)  AS Lag
FROM CountrySale;


SELECT *,
	LEAD(SaleTotal,2,-1) OVER(ORDER BY SalesAgent)  AS Lead,
	Lag(SaleTotal,2,-1) OVER(ORDER BY SalesAgent)  AS Lag,
	Lead(SaleTotal,1,-1) OVER(PARTITION BY SalesAgent ORDER BY SalesAgent)  AS Lead_Partition,
	Lag(SaleTotal,1,-1) OVER(PARTITION BY SalesAgent ORDER BY SalesAgent)  AS Lag_Partition
FROM CountrySale;