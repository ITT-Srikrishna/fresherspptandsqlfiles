--Row Number

SELECT ProductID,Name,Color, 
	ROW_NUMBER() OVER (ORDER BY ProductID) AS ROW_NUMBER			--Numbering rows in the order of ProductID
FROM Production.Product;


SELECT ProductID,Name,Color, 
	ROW_NUMBER() OVER (PARTITION BY Color ORDER BY Color) AS ROW_NUMBER		--Numbering rows partitioned by Color and 
FROM Production.Product														--in the order of Color
WHERE COLOR IS NOT NULL;


WITH ProductModified AS 
(
	SELECT *, ROW_NUMBER() OVER (PARTITION BY Color ORDER BY Color) AS ROW_NUMBER		--Deleting duplicate rows partitioned by Color 
	FROM Production.Product																--partitioned by Color 
)																						--and in the order of Color 
DELETE FROM ProductModified WHERE ROW_NUMBER > 90;										--where row number is greater than 90


-----------------------------------------------------------------------------------------------------------------------

--Ranking

SELECT ProductID,Name,StandardCost, 
	RANK() OVER (ORDER BY StandardCost) AS Ranking					--Ranking rows in the order of StandardCost
FROM Production.Product
WHERE StandardCost > 0;

SELECT ProductID,Name,StandardCost, 
	DENSE_RANK() OVER (ORDER BY StandardCost) AS DenseRank			--Dense Ranking rows in the order of StandardCost
FROM Production.Product
WHERE StandardCost > 0;

SELECT ProductID,Name,StandardCost, 
	RANK() OVER (ORDER BY StandardCost) AS Ranking,					--Dense Ranking and Ranking rows in the order of StandardCost
	DENSE_RANK() OVER (ORDER BY StandardCost) AS DenseRank
FROM Production.Product
WHERE StandardCost > 0;

WITH Result AS 
(
	SELECT ProductID,Name,StandardCost, 
		DENSE_RANK() OVER (ORDER BY StandardCost DESC) AS DenseRank
	FROM Production.Product
	WHERE StandardCost > 0
)
SELECT TOP 1 StandardCost FROM Result WHERE DenseRank = 4;			--Selecting first row with Rank 4 
																	--according to the dense rank



