--STORE PROCEDURES

SELECT * FROM Production.Product;

CREATE PROC spGetProduct					--Creating Store Procedure
AS
BEGIN
	SELECT ProductID,Name,Color,Size 
	FROM Production.Product
	WHERE Color='Red' OR Size='M'; 
END

EXECUTE spGetProduct;						--Executing Store Procedure

ALTER PROC spGetProduct						--Altering Store Procedure
AS
BEGIN
	SELECT ProductID,Name,Color,Size 
	FROM Production.Product
	WHERE Color='Red' OR Size='M'
	Order By Name;
END



CREATE PROC spGetProductByColorAndSize			--Store Procedure with arguments
@Color varchar(20),
@Size varchar(10)
AS
BEGIN
	SELECT ProductID,Name,Color,Size 
	FROM Production.Product
	WHERE Color=@Color OR Size=@Size;
END

EXECUTE spGetProductByColorAndSize 'Red','L';		--Executing Store procedure by passing arguments		

DROP PROC spGetProductByColorAndSize;							--Droping Store Procedure

ALTER PROC spGetProductByColorAndSize			--Alter Procedure with arguments
@Color nvarchar(20),
@Size nvarchar(10)
WITH ENCRYPTION
AS
BEGIN
	SELECT ProductID,Name,Color,Size 
	FROM Production.Product
	WHERE Color=@Color OR Size=@Size;
END

CREATE PROC spGetProductCountByColor			--Store Procedure with Output Parameter
@Color nvarchar(20),
@ProductCount int OUTPUT
AS
BEGIN 
	SELECT @ProductCount=Count(ProductId)
	FROM Production.Product
	Where Color=@Color; 
END

DECLARE @ProductCount int;
EXECUTE spGetProductCountByColor 'Red', @ProductCount out
Print @ProductCount


CREATE PROC spGetProduct1					--Creating Store Procedure with return value
AS
BEGIN
	RETURN(SELECT COUNT(ProductID) 
	FROM Production.Product
	WHERE Color='Red'); 
END

DECLARE @ProductCount int;
EXECUTE @ProductCount = spGetProduct1;		
Print @ProductCount;


sp_help spGetProduct;
sp_helptext spGetProduct;
sp_helptext spGetProductByColorAndSize