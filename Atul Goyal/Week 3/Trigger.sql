--TRIGGER'

CREATE TABLE emp
	(ID int PRIMARY KEY IDENTITY(1,1),
	Name nvarchar(30) NOT NULL,
	Phone nvarchar(10));

CREATE TABLE emp_Audit
	(ID int PRIMARY KEY IDENTITY(1,1),
	Phone nvarchar(100));

DROP TABLE emp; 
DROP TABLE emp_Audit;

SELECT * FROM emp;
SELECT * FROM emp_Audit;

CREATE TRIGGER tr_emp_insert				--After INSERT trigger
ON emp
FOR INSERT
AS
BEGIN
	--SELECT * FROM inserted;
	DECLARE @phone nvarchar(10);
	SELECT @phone=Phone FROM inserted;

	INSERT INTO emp_Audit 
	VALUES ('New phone no. ' + @phone + ' is added on ' + cast(GETDATE() as varchar(50)));

END


INSERT INTO emp VALUES('A','1234');
INSERT INTO emp VALUES('B','2345');
INSERT INTO emp VALUES('C','3456');
INSERT INTO emp VALUES('D','4567');


CREATE TRIGGER tr_emp_delete				--After DELETE trigger
ON emp
FOR DELETE
AS
BEGIN
	--SELECT * FROM deleted;
	DECLARE @id int;
	SELECT @id=ID FROM deleted;

	INSERT INTO emp_ 
	VALUES ('Employee with id ' + cast(@id as nvarchar(5)) + ' is deleted on ' + cast(GETDATE() as varchar(50)));

END

DELETE FROM emp WHERE id=1;
--DELETE FROM emp_Audit WHERE Phone is null;


CREATE TRIGGER tr_InsteadOf				--INSTEAD OF TRIGGER
ON HumanResources.Department
INSTEAD OF INSERT
AS
BEGIN
    PRINT('Trigger executed instead of INSERT')
END 

INSERT INTO HumanResources.Department (GroupName, Name)
VALUES ('A','A');


-----------------------------------------------------------------------------------------------------------------
--DDL TRIGGERS


CREATE TRIGGER DatabaseScopeTrigger					--DATABASE SCOPED TRIGGER
ON DATABASE                                 
FOR CREATE_TABLE,ALTER_TABLE,DROP_TABLE
AS
BEGIN
    Print 'You have either created,altered or dropped a table'
END

CREATE TABLE tbl(ID INT)
DROP TABLE tbl


--SEREVER SCOPED TRIGGER

CREATE TRIGGER ServerScopeTrigger
ON ALL SERVER                             
FOR CREATE_TABLE,ALTER_TABLE,DROP_TABLE
AS
BEGIN
    ROLLBACK
    Print 'You cannot create,alter or drop a table'
END

CREATE TABLE tbl(ID INT)



CREATE TRIGGER trgr1
ON DATABASE
FOR CREATE_TABLE
AS
BEGIN
  PRINT 'TRIGGER 1'
END

CREATE TRIGGER trgr2
ON DATABASE
FOR CREATE_TABLE
AS
BEGIN
  PRINT 'TRIGGER 2'
END

EXEC sp_settriggerorder							--Setting the order of the execution of triggers on the current database
@TRIGGERNAME='trgr2',
@ORDER='FIRST',
@STMTTYPE='CREATE_TABLE',
@NAMESPACE='DATABASE'

CREATE TABLE tbl(ID INT);
DROP TABLE tbl;


DISABLE TRIGGER ServerScopeTrigger ON ALL SERVER;		--Disablig the trigger

ENABLE TRIGGER ServerScopeTrigger ON ALL SERVER;		--Enabling the trigger

DROP TRIGGER ServerScopeTrigger ON ALL SERVER;			--Droping the trigger




