ALTER PROC spGetProductByColorAndSizeL			--Store Procedure with default arguments
@Color varchar(20),
@Size varchar(10) = 'L'
AS
BEGIN
	SELECT ProductID,Name,Color,Size 
	FROM Production.Product
	WHERE Color=@Color AND Size=@Size;
END

EXECUTE spGetProductByColorAndSizeL 'Black';	--Executing SP with default parameter 