CREATE FUNCTION fn_getEmpByDept(@DeptId int)
RETURNS TABLE
AS
RETURN 
(
	SELECT * 
	FROM HumanResources.EmployeeDepartmentHistory AS E
	WHERE E.DepartmentID=@DeptId
);

SELECT * FROM fn_getEmpByDept(1)

SELECT E.BusinessEntityID, F.LoginID, F.JobTitle, F.Gender, D.DepartmentID, D.Name
FROM HumanResources.Department D
CROSS APPLY fn_getEmpByDept(D.DepartmentID) E
INNER JOIN HumanResources.Employee F
ON E.BusinessEntityID=F.BusinessEntityID
ORDER BY E.BusinessEntityID;


SELECT E.BusinessEntityID, F.LoginID, F.JobTitle, F.Gender, D.DepartmentID, D.Name
FROM HumanResources.Department D
OUTER APPLY fn_getEmpByDept(D.DepartmentID) E
INNER JOIN HumanResources.Employee F
ON E.BusinessEntityID=F.BusinessEntityID
ORDER BY E.BusinessEntityID;