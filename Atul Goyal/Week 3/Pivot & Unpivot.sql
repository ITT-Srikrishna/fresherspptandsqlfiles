--PIVOT

CREATE TABLE CountrySale
	(SalesCountry nvarchar(50),
	SalesAgent nvarchar(50),
	SaleTotal int);

INSERT INTO CountrySale
VALUES ('India', 'A',876),
		('UK', 'A',764),
		('US', 'A',986),
		('US', 'B',234),
		('UK', 'B',928),
		('India', 'B',560),
		('US', 'C',527),
		('India', 'C',837),
		('UK', 'C',875);

DROP TABLE CountrySale;
SELECT * FROM CountrySale;

SELECT SalesAgent,SalesCountry,SUM(SaleTotal) AS TotalSale
FROM CountrySale
GROUP BY SalesAgent, SalesCountry;


SELECT SalesAgent,India,US,UK				--Query with PIVOT
FROM CountrySale
PIVOT										--PIVOT keyword
(
	SUM(SaleTotal)							--Agreegate function
	FOR SalesCountry						
	IN([India],[US],[UK])					--PIVOT columns
)
AS PivotTable;


SELECT 'AverageCost' AS AverageCost, [0],[1],[2],[3],[4]
FROM 
(SELECT StandardCost, DaysToManufacture								--Derived Table
FROM Production.Product) AS SourceTable
PIVOT
(
	AVG(StandardCost)
	FOR DaysToManufacture
	IN([0],[1],[2],[3],[4])
)
AS PivotTable;



-----------------------------------------------------------------------------------------------------------------

CREATE TABLE AgentSale
(SalesAgent nvarchar(20),
India int, US int, UK int);

TRUNCATE TABLE AgentSale;

INSERT INTO AgentSale
VALUES ('A',456,948,653),
	   ('B',876,754,875);


SELECT SalesAgent, Country, SalesAmount
FROM AgentSale
UNPIVOT						--UNPIVOT KEYWORD				
(
	SalesAmount				
	FOR Country				--Column for which unpivoting is being done
	IN (India,US,UK)
) AS UnpivotTable


