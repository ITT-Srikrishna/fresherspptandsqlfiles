--Cases

SELECT ProductId, Color=
	CASE Size
		WHEN '58' THEN 'Red'
		WHEN '42' THEN 'Silver'
		WHEN '44' THEN 'Black'
		
	END,
Size 
FROM SalesLT.Product;
	
SELECT ProductId, ListPrice
From SalesLT.Product
Order By CASE WHEN ListPrice < 365 THEN ListPrice END 
		,CASE WHEN ListPrice > 365 THEN ListPrice END DESC;


--VIEWS

CREATE VIEW [RED Products] AS
SELECT ProductID, Name, ProductNumber, Color
FROM SalesLT.Product
WHERE Color='Red';

SELECT * FROM [RED Products];
