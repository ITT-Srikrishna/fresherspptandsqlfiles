--Exception Handling

BEGIN TRY   
	SELECT 1/0; /* Error Occur Here */  
END TRY  
BEGIN CATCH  
	SELECT 
		ERROR_NUMBER() AS ErrorNumber,
		ERROR_SEVERITY() AS ErrorSeverity  
        ,ERROR_STATE() AS ErrorState  
        ,ERROR_PROCEDURE() AS ErrorProcedure  
        ,ERROR_LINE() AS ErrorLine  
        ,ERROR_MESSAGE() AS ErrorMessage;  
END CATCH   

--USER DEFINED EXCEPTION

Declare @val1 int;  
Declare @val2 int;  
BEGIN TRY  
	Set @val1=8;  
	Set @val2=@val1%2;   
	if @val1=1  
		Print ' Error Not Occur'  
	else  
		Begin  
		Print 'Error Occur';  
		Throw 60000,'Number Is Even',5       --Throwing User defined exception in Catch block
	End  
END TRY  
BEGIN CATCH  
	Print 'Error Occur that is:'  
	Print Error_Message()  
END CATCH   

--@@ERROR

SELECT * FROM SalesLT.Product;
IF @@ERROR = 547
	Print 10;
ELSE
	Print 20;


--Try catch in transactions

BEGIN TRANSACTION;  
  
BEGIN TRY  
    -- Generate a constraint violation error.  
    TRUNCATE TABLE SalesLT.Product;
END TRY  
BEGIN CATCH  
    SELECT   
        ERROR_NUMBER() AS ErrorNumber  
        ,ERROR_SEVERITY() AS ErrorSeverity  
        ,ERROR_STATE() AS ErrorState  
        ,ERROR_PROCEDURE() AS ErrorProcedure  
        ,ERROR_LINE() AS ErrorLine  
        ,ERROR_MESSAGE() AS ErrorMessage;  
  
    IF @@TRANCOUNT > 0  
        ROLLBACK TRANSACTION;  
END CATCH;  
  
IF @@TRANCOUNT > 0  
    COMMIT TRANSACTION;  