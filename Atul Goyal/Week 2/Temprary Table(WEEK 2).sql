--TEMPORARY TABLES

CREATE TABLE #ProductDetails 
	(ProductId int,
	Name varchar(50) NOT NULL,
	ProductNo nvarchar(50) NOT NULL,
	Color nvarchar(10),
	ListPrice money NOT NULL,
	Size nvarchar(10),
	ProductCategoryId int);


SELECT * FROM #ProductDetails

DROP TABLE #ProductDetails;

ALTER TABLE #ProductDetails
DROP COLUMN Size;

SELECT COUNT(*) FROM #ProductDetails;
SELECT COUNT(*) FROM SalesLT.Product;

BEGIN TRAN ;
DELETE FROM #ProductDetails
WHERE ProductId<708;
COMMIT TRAN ;

BEGIN TRAN ;
SAVE TRAN ABC
DELETE FROM #ProductDetails
WHERE ProductId=719;
ROLLBACK TRAN ABC;

--joins

SELECT COUNT(*) 
FROM SalesLT.Product as P 
INNER JOIN #ProductDetails as PD
ON P.ProductId=PD.ProductId;


SELECT * 
FROM #ProductDetails as PD
LEFT JOIN SalesLT.Product as P
ON P.ProductId=PD.ProductId;

--ISNULL(iai.Quantity,0)

SELECT * 
FROM SalesLT.Product as P 
RIGHT JOIN #ProductDetails as PD
ON P.ProductId=PD.ProductId;

SELECT * 
FROM #ProductDetails as PD
FULL JOIN SalesLT.Product as P
ON P.ProductId=PD.ProductId;

--subqueries
SELECT ProductId, Name
FROM #ProductDetails
WHERE ProductCategoryID IN
	(SELECT ProductCategoryID 
	FROM SalesLT.ProductCategory
	WHERE Name IN 
		(SELECT Name 
		FROM SalesLT.ProductCategory
		WHERE Name LIKE '%a%'));

SELECT P.ProductId, P.Name
FROM 
	(SELECT ProductID, Name
	FROM #ProductDetails
	WHERE Color='Red') as P
WHERE P.ProductId > 750;