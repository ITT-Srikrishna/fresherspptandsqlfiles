
CREATE TABLE Emp
	(ID int PRIMARY KEY,
	Name varchar(50) NOT NULL,
	Age int);

EXECUTE sp_helpindex Emp;		--Check indexing on the table

INSERT INTO Emp VALUES (3,'C',5);
INSERT INTO Emp VALUES (1,'A',4);
INSERT INTO Emp VALUES (4,'D',3);
INSERT INTO Emp VALUES (2,'B',2);
INSERT INTO Emp VALUES (5,'E',1);

SELECT * FROM Emp;

CREATE CLUSTERED INDEX ind_Emp           --CLUSTERED INDEXING
ON Emp(Age ASC);

DROP TABLE Emp;


CREATE NONCLUSTERED INDEX ind_Emp_Name    --NON-CLUSTERED INDEXING
ON Emp(Name ASC);

EXECUTE sp_helpindex Emp;      --Check indexing on the table

CREATE UNIQUE NONCLUSTERED INDEX UI_Emp_Name   --UNIQUE NON CLUSTURED INDEX
ON Emp(Name);

INSERT INTO Emp VALUES (6,'A',4);    -- Give error as Name coloumn have unique non-clustered index







