--Null Handling

CREATE TABLE EmpTable
	(Id int PRIMARY KEY,
	FirstName varchar(20),
	MiddleName varchar(20),
	LastName varchar(20));

INSERT INTO EmpTable
VALUES (1,'A',null,null),
		(2,'C','D',null),
		(3,null,'E','F'),
		(4,null,null,'G'),
		(5,'H','I','J');
INSERT INTO EmpTable
VALUES (6,null,null,null);

SELECT * FROM EmpTable;


--ISNULL() Function
SELECT ID,ISNULL(FirstName,00) AS FirstName,
		  ISNULL(MiddleName,00) AS MiddleName,
		  ISNULL(LastName,00) As LastName
FROM EmpTable;
		
--Coalesce() Function
SELECT ID, Coalesce(FirstName, MiddleName, LastName,'AllNull')
FROM EmpTable;

