--SQL Constraints

--NOT NULL Constraint
CREATE TABLE Student (
    ID int NOT NULL Primary Key,
    Name varchar(255) NOT NULL,
	Phone varchar(10) NOT NULL UNIQUE,
    Age int,
	CHECK (age >= 18)
);

INSERT INTO Student VALUES (1,'A',12345,20);
INSERT INTO Student VALUES (2,'B',23456,21);
INSERT INTO Student VALUES (3,'C',34567,20);

CREATE TABLE Test (
	ID int FOREIGN KEY REFERENCES Student(ID),
	TestID int NOT NULL,
	Marks int
);

INSERT INTO Test VALUES (1,12,80);
INSERT INTO Test VALUES (2,12,85);
INSERT INTO Test VALUES (3,12,75);

DROP TABLE Student;
DROP TABLE Test;


--NOT NULL VIOLATION

INSERT INTO Student				--can not insert null in not null column 
VALUES (1,NULL,12345,20);

--UNIQUE CONSTRAINTS

INSERT INTO Student				
VALUES (4,'D',12345,20);		--Can not insert duplicat balue in unique value column

--PRIMARY KEY VIOLATION

INSERT INTO Student VALUES		--Can not insert duplicat balue in primary key column
(1,'A',34567,20);

INSERT INTO Student VALUES		--Can not insert NULL in primary key column
(NULL,'A',34567,20);

--FOREIGN KEY VIOLATION

DROP TABLE Student;				--TEST table has a foriegn key from student table

--CHECK Constraint

INSERT INTO Student VALUES (5,'A',45678,10);	--Age value should be greater than 18

