           --TABLE VARIABLE

DECLARE @tvar TABLE 
	(PID int, 
	Name nvarchar(50), 
	Color nvarchar(50));

INSERT INTO @tvar
SELECT ProductID,Name,Color
FROM SalesLT.Product
WHERE Size='M';

SELECT * FROM @tvar

--joins
SELECT * 
FROM @tvar as tv 
INNER JOIN SalesLT.Product as P
ON P.ProductId=tv.PId;

SELECT * 
FROM @tvar as tv 
LEFT JOIN SalesLT.Product as P
ON P.ProductId=tv.PId;

SELECT * 
FROM @tvar as tv 
RIGHT JOIN SalesLT.Product as P
ON P.ProductId=tv.PId;

SELECT * 
FROM @tvar as tv 
OUTER JOIN SalesLT.Product as P
ON P.ProductId=tv.PId;

--subqueries
SELECT PId, Name
FROM @tvar
WHERE PID IN
	(SELECT ProductID 
	FROM SalesLT.Product
	WHERE Name IN 
		(SELECT Name 
		FROM SalesLT.Product
		WHERE Name LIKE 'H%'));




SELECT * FROM ##ProductDetails;